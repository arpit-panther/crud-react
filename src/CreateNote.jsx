import React,{useState} from 'react';


const CreateNote = (props) => {

    const [note,setNote] = useState({
        title:"",
        content:"",
    });


    const InputEvent = (event) => {
        const {name,value} = event.target;
        setNote((prevData)=>{
            return {
                ...prevData,
                [name]:value,
            }
        });
        console.log(note);
    };

    const addEvent = () => {
        props.passNote(note);
    };

    return (
        <>
            <div>
                <form>
                    <input type="text"
                           name="title"
                           value={note.title}
                           onChange={InputEvent}
                           placeholder="Title"/>

                    <textarea rows="" column=""
                              name="content"
                              value={note.content}
                              onChange={InputEvent}
                              placeholder="write a notes"/>

                    <button onClick={addEvent}>+</button>
                </form>
            </div>
        </>
    );
};

export default CreateNote;
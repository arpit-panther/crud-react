import React from 'react';
import { useParams } from "react-router-dom";

const Contact = () => {

    const {fname,lname} = useParams();

    return (
        <>
            <h1>{fname}{lname}</h1>
        </>
    );
}


export default Contact;
import React,{useState} from 'react';

const Inc_Dec = () => {

    const [count,countUpdate] = useState(0);


    const IncCount = () => {
        countUpdate(count+1);
    };
    const DecCount = () => {
        if(count>0){
            countUpdate(count-1);
        }
        else {
            alert('limit has been reached');
        }
    };

    return(
        <>
            <div className="main_div">
                <div className="center_div">
                    <h1>{count}</h1>
                    <div>
                        <button onClick={IncCount}> + </button>
                        <button onClick={DecCount}> - </button>
                    </div>
                </div>
            </div>
        </>
    )
};
export default Inc_Dec;
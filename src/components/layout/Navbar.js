import React, {components} from 'react';
import {Link,NavLink} from "react-router-dom";

const Navbar = () => {
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
           <div className="container">
               <div className="container-fluid">
                   <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                           aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                       <span className="navbar-toggler-icon"></span>
                   </button>
                   <div className="collapse navbar-collapse" id="navbarNav">
                       <ul className="navbar-nav">
                           <li className="nav-item">
                               <NavLink className="nav-link" aria-current="page" exact to="/">Home</NavLink>
                           </li>
                           <li className="nav-item">
                               <NavLink className="nav-link" exact to="/about">About</NavLink>
                           </li>
                           <li className="nav-item">
                               <NavLink className="nav-link" exact to="/contact">Contact</NavLink>
                           </li>
                       </ul>
                   </div>
                   <Link to="/users/add" className="btn btn-outline-light">Add User</Link>
               </div>
           </div>
        </nav>
    );
};

export default Navbar;
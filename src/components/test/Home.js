import React,{useState, useEffect} from 'react';
import axios from "axios";
import {Link} from 'react-router-dom';
import {Button} from "react-bootstrap";

import image from "../../images/2.jpg";

const Home = () => {

    const [users,setUser] = useState([]);

    useEffect(() => {
        loadUsers();
    },[]);

    const loadUsers = async () => {
        const users = JSON.parse(localStorage.getItem('users'));
        // const result = await axios.get("http://localhost:3003/users");
        // console.log(result)
        setUser(users);
    };

    function deleteUser (user) {
        let index = users.indexOf(user);
        users.splice(index,1)
        localStorage.setItem('users',JSON.stringify(users))
        loadUsers();
    };

    return(
        <div className="container">
            <div className="py-4">
                <h1>Home page</h1>
                <table className="table border shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">User Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        {/*<th scope="col">Image</th>*/}
                        <th scope="col">Gender</th>
                        <th scope="col">Address</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        users && users.map((user,index) => (
                            <tr>
                                <th scope="row">{index+1}</th>
                                <th>{user.name}</th>
                                <th>{user.username}</th>
                                <th>{user.email}</th>
                                <th>{user.phone}</th>
                                {/*file:///home/dev/Downloads/*/}
                                {/*<th><img src={require(`../../../../../../../Downloads/${user.image}`)}  height="100px" width="100px"/></th>*/}
                                {/*<th><img src={"../../images/"+${user.image})} height="100px" width="100px" /></th>*/}
                                <th>{user.gender}</th>
                                <th>{user.address}</th>
                                <td>
                                    <Link to={`/users/${index}`}>
                                        <Button className="btn btn-primary mr-2"> View </Button>
                                    </Link>
                                    <Link to={`/users/edit/${index}`}>
                                        <Button className="btn btn-warning mr-2"> Edit </Button>
                                    </Link>
                                    <Button className="btn btn-danger" onClick={() => deleteUser(user)}> Delete </Button>
                                </td>
                            </tr>
                        ))
                        }
                        </tbody>
                </table>
            </div>
        </div>
    );
};

export default Home;

import React, {useRef, useState, useEffect} from 'react';
import axios from "axios";
import {useHistory} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import classNames from 'classnames';
import {useForm} from "react-hook-form";

const AddUser = () => {

    let history = useHistory();
    const [hobbies, setHobbies] = useState([]);

    useEffect(() => {
        setUser({...user,hobbies:hobbies});
        return () => setUser({...user,hobbies:hobbies});
    }, [hobbies])

    const getValue = (e) => {
        let target = e.target;
        var value = target.value;

        if(hobbies.includes(value)){
            hobbies.pop(value);
        }
        else {
            hobbies.push(value);
        }
    }

    const fileUpload = (e) => {
        const file = e.target.files[0].name;
        setUser({...user,image:file});
    };

    const [user,setUser] = useState({
        name:"",
        username:"",
        email:"",
        phone:"",
        website:"",
        cpassword:"",
        hobbies:"",
        image:"",
        gender:"",
        address:"",
    });
    const {name,username,email,phone,website,cpassword,image,gender,address} = user;
    const onInputChange = event => {
        setUser({...user,[event.target.name]:event.target.value});
    };


    const onsubmit  = async (event) => {

        // event.preventDefault();


        if (localStorage.getItem("users") === null) {
            var users = [];
            users[0] = user;
            localStorage.setItem('users',JSON.stringify(users));
        }
        else {
            // var userItems = [];
            var userItems = JSON.parse(localStorage.getItem("users"));
            userItems.push(user);
            localStorage.setItem('users',JSON.stringify(userItems));
        }
        history.push("/");
    };



    const {register,handleSubmit,errors,watch} = useForm({
        mode:"onTouched",
    });

    const password = useRef({});
    password.current = watch("password", "");

    return(
        <div className="container">
            <div className="py-4">
                <h1>Add User</h1>
                <form onSubmit={handleSubmit( event => onsubmit(event))}>
                    <div className="mb-3">
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.name, })}
                               name="name" value={name}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:4,
                                       message:"Name should be more than 4 characters",
                                   },
                                   maxLength:{
                                       value:20,
                                       message:"Name should be less than 20 characters",
                                   },
                               })}
                               placeholder="Enter Your Name Here" />
                        {errors.name && (
                            <div className="invalid-feedback">
                                {errors.name.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.username, })}
                               name="username"
                               value={username}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:4,
                                       message:"User Name should be more than 4 characters",
                                   },
                                   maxLength:{
                                       value:20,
                                       message:"Name should be less than 20 characters",
                                   },
                               })}
                               placeholder="Enter Your UserName Here" />
                        {errors.username && (
                            <div className="invalid-feedback">
                                {errors.username.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.email, })}
                               name="email"
                               value={email}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   pattern:{
                                       value:/^[a-zA-Z.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.['com' {|} 'in']+)*$/,
                                       message:"please enter a valid email"
                                   },
                               })}
                               placeholder="Enter Your email Here" />
                        {errors.email && (
                            <div className="invalid-feedback">
                                {errors.email.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.phone, })}
                               name="phone"
                               value={phone}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   pattern:{
                                       value:/^\+(?:[0-9] ?){11}[0-9]$/,
                                       message:"please enter a valid Mobile Number"
                                   },
                               })}
                               placeholder="Enter Your Mobile Number With Country code Example: +91 9737324317" />
                        {errors.phone && (
                            <div className="invalid-feedback">
                                {errors.phone.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.website, })}
                               name="website"
                               value={website}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   maxLength:{
                                       value:20,
                                       message:"Name should be less than 20 characters",
                                   },
                               })}
                               placeholder="Enter Your Website Here" />
                        {errors.website && (
                            <div className="invalid-feedback">
                                {errors.website.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <input type="password"
                               className={classNames("form-control",{"is-invalid":errors.password, })}
                               name="password"
                            // value={password}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:8,
                                       message:"Name should be more than 8 characters",
                                   },
                               })}
                               placeholder="Enter Your Password" />
                        {errors.password && (
                            <div className="invalid-feedback">
                                {errors.password.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <input type="password"
                               className={classNames("form-control",{"is-invalid":errors.cpassword, })}
                               name="cpassword"
                               value={cpassword}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   validate:value =>
                                       value === password.current || "The Password Does Not Match"
                               })}
                               placeholder="Enter Your Confirm Password" />
                        {errors.cpassword && (
                            <div className="invalid-feedback">
                                {errors.cpassword.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <input type="file"
                               className={classNames("form-control",{"is-invalid":errors.file, })}
                               name="image"
                               onChange={fileUpload}
                        />
                    </div>
                    <div className="mb-3">
                        <label> Please select a Gender </label><br/>
                        <input type="radio"
                               value="male"
                               id="male"
                               name="gender"
                               ref={register({
                                   required:"This Field is Required",
                               })}
                               onChange={event => onInputChange(event)}
                        />Male
                        <input type="radio" value="female" id="female" name="gender" onChange={event => onInputChange(event)} ref={register({required:true,})} required />Female
                        {errors.gender && (
                            <div className="invalid-feedback">
                                {errors.gender.message}
                            </div>
                        )}
                    </div>
                    <div className="mb-3">
                        <label> Please select a Hobbies </label><br/>
                        <Checkbox
                            color="primary"
                            value="reading"
                            onChange={(e) => getValue(e)}
                        />Reading
                        <Checkbox
                            color="primary"
                            value="coding"
                            onChange={(e) => getValue(e)}
                        />Coding
                        <Checkbox
                            color="primary"
                            value="dancing"
                            onChange={(e) => getValue(e)}
                        />dancing
                        <Checkbox
                            color="primary"
                            value="cooking"
                            onChange={(e) => getValue(e)}
                        />Cooking
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.address, })}
                               name="address"
                               value={address}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:20,
                                       message:"Name should be more than 20 characters",
                                   },
                                   maxLength:{
                                       value:200,
                                       message:"Name should be less than 200 characters",
                                   },
                               })}
                               placeholder="Enter Your Address" />
                        {errors.address && (
                            <div className="invalid-feedback">
                                {errors.address.message}
                            </div>
                        )}
                    </div>
                    <button type="submit" className="btn btn-outline-primary btn-block">Submit</button>
                </form>
            </div>
        </div>
    );
};

export default AddUser;

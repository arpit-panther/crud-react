import React, {useState, useEffect, useRef} from 'react';
import axios from "axios";
import {useHistory, useParams} from 'react-router-dom';
import {Checkbox} from "@material-ui/core";
import classNames from "classnames";
import {useForm} from "react-hook-form";


const EditUser = ({match}) => {
    let params = match.params;
    let test = 'dfdfdfd';
    let history = useHistory();
    const [user,setUser] = useState({
        name:"",
        username:"",
        email:"",
        phone:"",
        website:"",
        cpassword:"",
        hobbies:"",
        image:"",
        gender:"",
        address:"",
    });
    const [hobbies, setHobbies] = useState([]);

    useEffect(() => {
        console.log(user);

        // return () => setUser({...user,hobbies:hobbies});
    }, [user])

    let tempHobbies = [];
    const getValue = (e) => {
        let target = e.target;
        var value = target.value;
        tempHobbies = user.hobbies;
        if(tempHobbies.includes(value)){
            let store =  tempHobbies.filter(hobby => hobby!==value);
            tempHobbies = store;
        }
        else {
            tempHobbies.push(value);
        }
        setUser({...user,hobbies: tempHobbies});
    }

    const {id} = useParams();

    const {name,username,email,phone,website,cpassword,image,gender,address} = user;
    const onInputChange = event => {
        setUser({...user,[event.target.name]:event.target.value});
    };

    useEffect(() => {
        loadUser();
    },[]);


    const fileUpload = (e) => {
        const file = e.target.files[0].name;
        setUser({...user,image:file});
    };


    const onsubmit  = async (event) => {
        event.preventDefault();
        const result = JSON.parse(localStorage.getItem('users'));
        result[params.id] = user;
        localStorage.setItem('users',JSON.stringify(result));
        history.push("/");

    };

    function loadUser() {
        const result = JSON.parse(localStorage.getItem('users'));
        var user = result[params.id];
        setUser(user);
    };

    const {register,handleSubmit,errors,watch} = useForm({
        mode:"onTouched",
    });

    const password = useRef({});
    password.current = watch("password", "")


    return(
        <div className="container">
            <div className="py-4">
                <h1>Edit User</h1>
                <form onSubmit={event => onsubmit(event)}>
                    <div className="mb-3">
                        <input type="text"
                               className="form-control"
                               name="name"
                               value={name}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:4,
                                       message:"Name should be more than 4 characters",
                                   },
                                   maxLength:{
                                       value:20,
                                       message:"Name should be less than 20 characters",
                                   },
                               })}
                               placeholder="Enter Your Name Here" />
                               {errors.name && (
                                   <div className="invalid-feedback">
                                       {errors.name.message}
                                   </div>
                               )}
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className="form-control"
                               name="username"
                               value={username}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:4,
                                       message:"User Name should be more than 4 characters",
                                   },
                                   maxLength:{
                                       value:20,
                                       message:"Name should be less than 20 characters",
                                   },
                               })}
                               placeholder="Enter Your UserName Here" />
                               {errors.username && (
                                   <div className="invalid-feedback">
                                       {errors.username.message}
                                   </div>
                               )}
                    </div>
                    <div className="mb-3">
                        <input type="email"
                               className="form-control"
                               name="email"
                               value={email}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   pattern:{
                                       value:/^[a-zA-Z.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.['com' {|} 'in']+)*$/,
                                       message:"please enter a valid email"
                                   },
                               })}
                               placeholder="Enter Your email Here" />
                               {errors.email && (
                                   <div className="invalid-feedback">
                                       {errors.email.message}
                                   </div>
                               )}
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className="form-control"
                               name="phone"
                               value={phone}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   pattern:{
                                       value:/^\+(?:[0-9] ?){11}[0-9]$/,
                                       message:"please enter a valid Mobile Number"
                                   },
                               })}
                               placeholder="Enter Your Phone Here" />
                               {errors.phone && (
                                   <div className="invalid-feedback">
                                       {errors.phone.message}
                                   </div>
                               )}
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className="form-control"
                               name="website"
                               value={website}
                               onChange={event => onInputChange(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:8,
                                       message:"Name should be more than 8 characters",
                                   },
                               })}
                               placeholder="Enter Your Website Here" />
                               {errors.password && (
                                   <div className="invalid-feedback">
                                       {errors.password.message}
                                   </div>
                               )}
                    </div>
                    {/*<div className="mb-3">*/}
                    {/*    <input type="password"*/}
                    {/*           className={classNames("form-control",{"is-invalid":errors.password, })}*/}
                    {/*           name="password"*/}
                    {/*        value={cpassword}*/}
                    {/*           onChange={event => onInputChange(event)}*/}
                    {/*           ref={register({*/}
                    {/*               required:"This Field is Required",*/}
                    {/*               minLength:{*/}
                    {/*                   value:8,*/}
                    {/*                   message:"Name should be more than 8 characters",*/}
                    {/*               },*/}
                    {/*           })}*/}
                    {/*           placeholder="Enter Your Password" />*/}
                    {/*    {errors.password && (*/}
                    {/*        <div className="invalid-feedback">*/}
                    {/*            {errors.password.message}*/}
                    {/*        </div>*/}
                    {/*    )}*/}
                    {/*</div>*/}
                    {/*<div className="mb-3">*/}
                    {/*    <input type="password"*/}
                    {/*           className={classNames("form-control",{"is-invalid":errors.cpassword, })}*/}
                    {/*           name="cpassword"*/}
                    {/*           value={cpassword}*/}
                    {/*           onChange={event => onInputChange(event)}*/}
                    {/*           ref={register({*/}
                    {/*               validate:value =>*/}
                    {/*                   value === password.current || "The Password Does Not Match"*/}
                    {/*           })}*/}
                    {/*           placeholder="Enter Your Confirm Password" />*/}
                    {/*    {errors.cpassword && (*/}
                    {/*        <div className="invalid-feedback">*/}
                    {/*            {errors.cpassword.message}*/}
                    {/*        </div>*/}
                    {/*    )}*/}
                    {/*</div>*/}
                    <div className="mb-3">
                        <input type="file"
                               // className={classNames("form-control",{"is-invalid":errors.file, })}
                               name="image"
                               onChange={fileUpload}
                        />
                    </div>
                    <div className="mb-3">
                        <label> Please select a Gender </label><br/>
                        <input type="radio"
                               value="male"
                               id="male"
                               name="gender"
                               onChange={event => onInputChange(event)}
                                checked={gender === "male"}
                        />Male
                        <input type="radio"
                               value="female"
                               id="female"
                               name="gender"
                               onChange={event => onInputChange(event)}
                               checked={gender === "female"}
                        />Female
                    </div>
                    <div className="mb-3">
                        <label> Please select a Hobbies </label><br/>
                        <Checkbox
                            color="primary"
                            value="reading"
                            checked={user.hobbies.includes("reading")}
                            onChange={(e) => getValue(e)}
                        />Reading
                        <Checkbox
                            color="primary"
                            value="coding"
                            checked={user.hobbies.includes("coding")}
                            onChange={(e) => getValue(e)}
                        />Coding
                        <Checkbox
                            color="primary"
                            value="dancing"
                            checked={user.hobbies.includes("dancing")}
                            onChange={(e) => getValue(e)}
                        />dancing
                        <Checkbox
                            color="primary"
                            value="cooking"
                            checked={user.hobbies.includes("cooking")}
                            onChange={(e) => getValue(e)}
                        />Cooking
                    </div>
                    <div className="mb-3">
                        <input type="text"
                               className="form-control" name="address" value={address} onChange={event => onInputChange(event)}
                               placeholder="Enter Your Website Here" />
                    </div>
                    <button type="submit"
                            className="btn btn-outline-primary btn-block">Submit</button>
                </form>
            </div>
        </div>
    );
};

export default EditUser;

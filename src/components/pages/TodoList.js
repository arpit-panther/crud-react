// import React,{useContext} from 'react';
// import {Button, ListGroup, ListGroupItem} from "react-bootstrap";
// import {Link,NavLink} from "react-router-dom";
// import {GlobalContext} from "../../context/GlobalState";
//
// export const TodoList = () => {
//     const {datas, removeTodo } = useContext(GlobalContext);
//     return(
//         <ListGroup className="mt-4">
//         {datas.map((data,index)=>(
//             <ListGroupItem key={index} className="d-flex">
//                 <strong>{data.name}</strong>
//                 <div className="ml-auto">
//                     <Link className="btn btn-warning pr-1" to={`/EditTodo/${data.id}`}>
//                         Edit
//                     </Link>
//                     <Button onClick={() =>
//                         removeTodo(data.id)}
//                         className="btn btn-danger">Delete</Button>
//                 </div>
//             </ListGroupItem>
//         ))}
//         </ListGroup>
//     )
// }
// export default TodoList;

import React, {useState} from 'react';
import useLocalStorage from "../../../src/hooks/useLocalStorage";


const Form = () => {

    const [form,setForm] = useLocalStorage("data",[

    ]);

    const formSubmit = () => {

    };

    return(
        <div className="container">
            <div className="py-4">
                <h1>Add To Do</h1>
                <form>
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">Add Task</label>
                        <input type="email" className="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp"/>
                    </div>
                    <button type="submit" className="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    );
};


export default Form;
import React,{useState , useEffect} from 'react';
import axios from "axios";
import {useHistory, useParams} from 'react-router-dom';

const EditUser = ({match}) => {
    let params = match.params;


    let history = useHistory();
    const {id} = useParams();
    const [user,setUser] = useState({
        name:"",
        username:"",
        email:"",
        phone:"",
        website:""
    });
    const {name,username,email,phone,website} = user;
    const onInputChange = event => {
        setUser({...user,[event.target.name]:event.target.value});
    };

    useEffect(() => {
        loadUser();
    },[]);


    const onsubmit  = async (event) => {
        event.preventDefault();
        const result = JSON.parse(localStorage.getItem('users'));
        result[params.id] = user;
        localStorage.setItem('users',JSON.stringify(result));
        history.push("/");

        // const result = JSON.parse(localStorage.getItem('users'));
        // var user = result[params.id];
        //
        // user = event;
        //
        //  console.log(user);
        //
        // let id = params.id;
        // console.log(id);
        // await axios.put(`http://localhost:3003/users/${id}`, user);
        // history.push("/");
    };

    function loadUser() {

        const result = JSON.parse(localStorage.getItem('users'));
        var user = result[params.id];
        setUser(user);
    };

    return(
        <div className="container">
            <div className="py-4">
                <h1>Edit User</h1>
                <form onSubmit={event => onsubmit(event)}>
                    <div className="mb-3">
                        <input type="text" className="form-control" name="name" value={name} onChange={event => onInputChange(event)} placeholder="Enter Your Name Here" />
                    </div>
                    <div className="mb-3">
                        <input type="text" className="form-control" name="username" value={username} onChange={event => onInputChange(event)} placeholder="Enter Your UserName Here" />
                    </div>
                    <div className="mb-3">
                        <input type="email" className="form-control" name="email" value={email} onChange={event => onInputChange(event)} placeholder="Enter Your email Here" />
                    </div>
                    <div className="mb-3">
                        <input type="text" className="form-control" name="phone" value={phone} onChange={event => onInputChange(event)} placeholder="Enter Your Phone Here" />
                    </div>
                    <div className="mb-3">
                        <input type="text" className="form-control" name="website" value={website} onChange={event => onInputChange(event)} placeholder="Enter Your Website Here" />
                    </div>
                    <button type="submit" className="btn btn-outline-primary btn-block">Submit</button>
                </form>
            </div>
        </div>
    );
};

export default EditUser;
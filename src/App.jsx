//importb except pages and components

// import React, {useState,useEffect} from "react";
// import {createContext} from "istanbul-lib-report";
// import axios from 'axios';
import { BrowserRouter as Router, Route,Switch} from "react-router-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.css";


//import pages and components only.

// import ToDoList from "./ToDoList";
// import Header from "./Header";
// import Footer from "./Footer";
// import CreateNote from "./CreateNote";
// import Note from "./Notes";
// import ComA from './ComA';
// import About from "./About";
// import Contact from "./Contact";
// import Error from "./Error";
// import Menu from "./Menu";
// import Form from "./Form";
import Home from "./components/pages/Home";
import About from "./components/pages/About";
import Contact from "./components/pages/Contact";
import Navbar from "./components/layout/Navbar";
import EditTodo from "./components/pages/EditTodo";
import NotFound from "./components/pages/NotFound";
import AddUser from "./components/users/AddUser";
import EditUser from "./components/users/EditUser";
import User from "./components/users/User";

// const App = () => {
//     const state = useState();
//
//     const [count,setCount] = useState(0);
//
//     const IncNum = () => {
//         setCount(count+1);
//         console.log('clicked');
//     }
//     return(
//         <>
//             <h1>{count}</h1>
//             <button onClick={IncNum}>Click Me</button>
//         </>
//     )
// }


// const App = () =>{
//
//     let time = new Date().toLocaleTimeString();
//
//     const [ctime,setTime] = useState(time);
//
//     const UpdateTime = () =>{
//         time = new Date().toLocaleTimeString();
//         setTime(time);
//     }
//
//     setInterval(UpdateTime,1000);
//
//     return(
//         <>
//             <h1>{ctime}</h1>
//             <button onClick={UpdateTime}>Get Time</button>
//         </>
//     )
// }


// const App = () =>{
//     const purple = "#8e44ad";
//     const [bg,bgUpdate] = useState(purple);
//
//     const BGChange = () =>{
//         let bg = "#008140";
//         bgUpdate(bg);
//     }
//
//     return(
//         <>
//             <div style={ { backgroundColor : bg } }>
//                 <button onClick={BGChange}>Click Me</button>
//             </div>
//         </>
//     );
// }


// const App = () => {
//
//     const [fullName,setFullName] = useState({
//         fname : '',
//         lname : ''
//     });
//     const InputEvent = (event) => {
//         console.log(event.target.value);
//
//         const value = event.target.value;
//         const name = event.target.value;
//
//
//         setFullName((preValue) => {
//             if(name === "fName"){
//                 return {
//                     fname : value,
//                     lname : preValue.lname,
//                 };
//             }else if(name === "lname"){
//                 return {
//                     fname : preValue.fname,
//                     lname : value,
//                 };
//             }
//         });
//     };
//
//     const onSubmits = (event) => {
//         event.preventDefault();
//         alert('submitted');
//     };
//     return(
//         <>
//             <div className="main_div">
//                 <form onSubmit={onSubmits}>
//                     <div>
//                         <h1>Hello {fullName.fname} {fullName.lname}</h1>
//                         <input
//                             type="text"
//                             placeholder="Enter Your Name"
//                             onChange={InputEvent}
//                             name='fName'
//                             value={fullName.fname}
//                         />
//                         <input
//                             type="text"
//                             placeholder="Enter Your Last Name"
//                             onChange={InputEvent}
//                             name='lName'
//                             value={fullName.lname}
//                         />
//                         <button type="submit"> Submit </button>
//                     </div>
//                 </form>
//             </div>
//         </>
//     );
// }


// to-do-list
// const App = () => {
//
//     const [inputlist , setInputlist] = useState();
//     const [items,setItems] = useState([]);
//
//     const itemEvent = (event) => {
//         // console.log(event.target.value);
//         setInputlist(event.target.value);
//     };
//     const listofItems = () => {
//         setItems((olditems) => {
//             return [...olditems,inputlist];
//         });
//         setInputlist('');
//     };
//     const deleteItems = (id) => {
//         console.log('clicked');
//
//         setItems((olditems) => {
//             return olditems.filter((arrElem,index) => {
//                 return index !== id;
//             });
//         });
//     };
//
//     return(
//         <>
//             <div className="main_div">
//                 <div className="center_div">
//                     <br/>
//                     <h1>TO DO List</h1>
//                     <br/>
//                     <input type="text"
//                            placeholder="Add A Items"
//                            value={inputlist}
//                            onChange={itemEvent}
//                     />
//                     <button onClick={listofItems}>+</button>
//                     <ol>{
//                         items.map( (itemval,index) => {
//                             return(
//                                 <ToDoList
//                                     key={index}
//                                     id={index}
//                                     text={itemval}
//                                     onSelect={deleteItems}
//                                 />
//                             );
//                         })
//                     }
//                     </ol>
//                 </div>
//             </div>
//         </>
//     );
// };


//google keep

// const App = () => {
//
//     const [addItem,setAddItem] = useState();
//
//
//     const addNote = (note) => {
//         setAddItem((prevData) => {
//             return [...prevData,note];
//         });
//         console.log(note);
//     };
//
//     return(
//       <>
//           <Header/>
//           <CreateNote passNote={addNote}/>
//           {
//               addItem.map((val,index) => {
//               return(
//                   <Note
//                     key={index}
//                     id={index}
//                     title={val.title}
//                     content={val.content}
//                 />
//               );
//                 })
//           }
//           <Footer/>
//
//       </>
//     );
// };



// const FirstName = createContext();
//
// const App = () => {
//     return (
//         <>
//             <FirstName.Provider value={"Arpit"}>
//                 <ComA/>
//             </FirstName.Provider>
//          </>
//     );
// };
//
//
// export default App;
// export {FirstName};



// const App = () => {
//
//     const [num,setNum] = useState(0);
//     useEffect(() => {
//         alert('clicked');
//     });
//
//     return(
//         <>
//             <button onClick={() => {
//                 setNum(num+1);
//             }}>+{num}</button>
//         </>
//     );
// };


//chnage title

// const App = () => {
//
//     const [num,setNum] = useState(0);
//
//     useEffect(() => {
//         document.title=`${num}`
//     });
//
//
// return(
//     <>
//         <button onClick={() => {
//             setNum(num+1)
//         }}>{num}+</button>
//     </>
// );
// };


//Pikachu Api

// const App = () => {
//
//     const[num,setNum] = useState(0);
//     const[name,setName] = useState();
//     const[moves,setMoves] = useState();
//
//     useEffect(() => {
//         async function getData(){
//             const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${num}`);
//             setName(res.data.name);
//             setMoves(res.data.moves.length);
//         }
//
//         getData();
//     });
//
//
//     return (
//         <>
//             <h1>You Select {num}</h1>
//             <h1>Name {name}</h1>
//             <h1>Moves {moves}</h1>
//             <select value={num} onChange={(event) => {
//                 setNum(event.target.value);
//             }}>
//                 <option value="1">1</option>
//                 <option value="25">25</option>
//                 <option value="3">3</option>
//                 <option value="4">4</option>
//                 <option value="5">5</option>
//             </select>
//         </>
//     );
// };


// const App = () => {
// return(
//         <>
//             <Menu/>
//             <Switch>
//                 <Route exact path="/" component={About}/>
//                 <Route path="/contact/:fname/:lname" component={Contact}/>
//                 <Route component={Error}/>
//             </Switch>
//         </>
//     );
// };



function App(){
    return (

        <Router>
            <div className="App">
                <Navbar/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/EditTodo/:id" component={EditTodo}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/contact" component={Contact}/>
                    <Route exact path="/users/add" component={AddUser}/>
                    <Route exact path="/users/edit/:id" component={EditUser}/>
                    <Route exact path="/users/:id" component={User}/>
                    <Route component={NotFound}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;

